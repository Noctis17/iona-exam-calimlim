<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class PetController extends Controller
{

    public const DOG_API = "https://api.thedogapi.com/v1/images/search";
    public const CAT_API = "https://api.thecatapi.com/v1/images/search";

    /**
     * Show combined results from Dog and Cat APIs including all details
     *
     * @return Response
     */
    public function listAll(Request $request)
    {
        // Leave a default value of the variable has not given a value
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 10;

        // Randomize to choose either dog or cat
        $arrLimit = $this->randomizer($limit);

        $dogResults = $this->requestAPI(self::DOG_API, env('DOG_API_KEY'), $page, $arrLimit['dogLimit']);
        $catResults = $this->requestAPI(self::CAT_API, env('CAT_API_KEY'), $page, $arrLimit['catLimit']);
        $results = array_merge($dogResults, $catResults);

        // Shuffle the content of $results for randomized contents
        shuffle($results);

        $apiResponse['page'] = $page;
        $apiResponse['limit'] = $limit;
        $apiResponse['results'] = $results;

        return json_encode($apiResponse);
    }

    /**
     * Show combined results from Dog and Cat APIs.
     *
     * @return Response
     */
    public function listCombined(Request $request)
    {
        // Leave a default value of the variable has not given a value
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 10;

        // Randomize to choose either dog or cat
        $arrLimit = $this->randomizer($limit);

        $dogResults = $this->requestAPI(self::DOG_API, env('DOG_API_KEY'), $page, $arrLimit['dogLimit'], 1);
        $catResults = $this->requestAPI(self::CAT_API, env('CAT_API_KEY'), $page, $arrLimit['catLimit'], 1);
        $results = array_merge($dogResults, $catResults);

        // Shuffle the content of $results for randomized contents
        shuffle($results);

        $apiResponse['page'] = $page;
        $apiResponse['limit'] = $limit;
        $apiResponse['results'] = $results;

        return json_encode($apiResponse);
    }

    /**
     * Show separated results from Dog and Cat APIs.
     *
     * @return Response
     */
    public function listByBreed(Request $request)
    {

        // Leave a default value of the variable has not given a value
        $page = $request->input('page') ?? 1;
        $limit = $request->input('limit') ?? 10;

        // Randomize to choose either dog or cat
        $arrLimit = $this->randomizer($limit);
        
        $results["dogs"] = $this->requestAPI(self::DOG_API, env('DOG_API_KEY'), $page, $arrLimit['dogLimit'], 1);
        $results["cats"] = $this->requestAPI(self::CAT_API, env('CAT_API_KEY'), $page, $arrLimit['catLimit'], 1);

        $apiResponse['page'] = $page;
        $apiResponse['limit'] = $limit;
        $apiResponse['results'] = $results;

        return json_encode($apiResponse);
    }

    /**
     * Show 1 image of either a dog or cat
     *
     * @return Response
     */
    public function listByImage(Request $request)
    {

        // Leave a default value of the variable has not given a value
        $page = 1;
        $limit = 1;

        // Randomize to choose either dog or cat
        $rand = rand(0,1);

        // Cat would be the default value
        $api = self::CAT_API;
        $api_key = env('CAT_API_KEY');

        // Change  to dog if 1, avoid using else statement
        if ($rand) {
            $api = self::DOG_API;
            $api_key = env('DOG_API_KEY');
        }

        $results = $this->requestAPI($api, $api_key, $page, $limit, 1);

        return json_encode($results);
    }

    /*
    *   Randomizes either dog or cat will get equal limits
    *
    *   @return array $arrLimit
    */
    public function randomizer($limit){

        // Let the computer randomize between 0 or 1
        $rand = rand(0,1);

        $dogLimit = ceil($limit / 2);
        $catLimit = floor($limit / 2);

        if ($rand) {
            $dogLimit = floor($limit / 2);
            $catLimit = ceil($limit / 2);
        }

        $arrLimit['dogLimit'] = $dogLimit;
        $arrLimit['catLimit'] = $catLimit;

        return $arrLimit;
    }

    /* 
    *   Function to call a request to the third party API Dogs and Cats
    *
    *   @return Response
    */
    public function requestAPI($api, $key, $page, $limit, $removeKeys = 0) {
        // Construct base string structure for the URL
        $apiString = $api."?page=".$page."&limit=".$limit;

        // Call the HTTP helper from laravel
        // Convert it to a JSON formatted response
        $response = json_decode(Http::withHeaders([
            'x-api-key' => $key,
        ])->get($apiString));

        // Remove unnecessary keys
        if ($removeKeys) {
            for($i = 0; $i < count($response); $i++){ 
                unset($response[$i]->breeds);
                unset($response[$i]->categories);
            }
        }

        return $response;
    }
}
